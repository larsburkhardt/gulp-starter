import gulp from 'gulp';
import gulpsass from 'gulp-sass';
import browserSync from 'browser-sync';
import autoprefixer from 'gulp-autoprefixer';
import sourcemaps from 'gulp-sourcemaps';
import cleanCSS from 'gulp-clean-css';
import yargs from 'yargs';
import gulpif from 'gulp-if';
import imagemin from 'gulp-imagemin';
import del from 'del';
import webpack from 'webpack-stream';
import named from 'vinyl-named';
import sass from 'sass';

gulpsass.compiler = sass;


const server = browserSync.create();

const PRODUCTION = yargs.argv.prod;


// Browser Sync
export const serve = (done) => {
    server.init({
        server: "./"
    });
    done();
}

export const reload = (done) => {
    server.reload();
    done();
}

// Clean up dist folder (gulp clean)
export const clean = () => del(['dist']);


const paths = {
    styles: {
        src: 'src/scss/styles.scss',
        dest: 'dist/css'
    },
    images: {
        src: 'src/images/**/*.{jpg,jpeg,png,svg,gif}',
        dest: 'dist/images'
    },
    scripts: {
        src: 'src/js/scripts.js',
        dest: 'dist/js'
    },
    other: {
        src: ['src/**/*', '!src/{images,js,scss}', '!src/{images,js,scss}/**/*'],
        dest: 'dist'
    },

}

// Styles Task
export const styles = () => {
    return gulp.src(paths.styles.src)
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(gulpsass().on('error', gulpsass.logError))
        .pipe(autoprefixer({  cascade: false, grid: true  }))
        .pipe(gulpif(PRODUCTION, cleanCSS({ compatibility: 'ie8' })))
        .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(server.stream());
}

// images task (gulp images)

export const images = () => {
    return gulp.src(paths.images.src)
        .pipe(gulpif(PRODUCTION, imagemin()))
        .pipe(gulp.dest(paths.images.dest));
}

// copy other folders (font etc) to dist (gulp copy)

export const copy = () => {
    return gulp.src(paths.other.src)
        .pipe(gulp.dest(paths.other.dest));
}

// scripts task (gulp scripts)

export const scripts = () => {
    return gulp.src(paths.scripts.src)
        .pipe(named())
        .pipe(webpack({
            module: {
                rules: [
                    {
                        test: /\.js$/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env']
                            }
                        }
                    }
                ]
            },
            output: {
                filename: '[name].js'
            },
            devtool: !PRODUCTION ? 'inline-source-map' : false,
            mode: PRODUCTION ? 'production' : 'development' //add this
        }))
        .pipe(gulp.dest(paths.scripts.dest));
}

export const watch = () => {
    gulp.watch('*.html', reload);
    gulp.watch('src/scss/**/*.scss', styles);
    gulp.watch('src/js/**/*.js', gulp.series(scripts, reload));
    gulp.watch(paths.images.src, gulp.series(images, reload));
    gulp.watch(paths.other.src, gulp.series(copy, reload));
}

// The final Task
export const dev = gulp.series(clean, gulp.parallel(styles, scripts, images, copy), serve, watch);
// The default task
export default dev;

// Default Task is "gulp dev" or "npm run start"
// Production output with imagemin: "npm run build"