# Gulp 4 Starter Kit

This Gulp Starter Kit is inspired by the famous Ali Alaa.
It features Sass Compilation with Auto Prefixing, Sourcemaps, BrowserSync and so on.

JavaScript ES6 Syntax is supported, your files get bundled.
Also included is image minification when running in production mode.

##Requirements:
- [Node.js](https://nodejs.org)
- Gulp CLI: `npm i -g gulp-cli`

## How to use
First, download the repository or clone it. Start the console from the directory and run `npm install`, so that the dependencies get installed.
After that, run `npm run start`or `gulp dev` to start development mode.
For the production mode with image minification, run `npm run build`.

Enjoy and happy coding,
Lars